using Xunit;
using FluentAssertions;

namespace Tests
{
    public class SampleTest
    {
        [Fact]
        public void NumberAssertions()
        {
            1.Should().Be(1);
            2.Should().BePositive().And.NotBe(1);
        }

        [Theory]
        [InlineData("String1", "String2")]
        public void StringAssertions(string s1, string s2)
        {
            s1.Should().Contain("String1").And.HaveLength(7);
            s2.Should().EndWith("ng2", "because it should be 'String2'");
        }
    }
}
using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using HumbleProgrammer.Characters;

namespace HumbleProgrammer
{
    [ExcludeFromCodeCoverage]
    public class Game
    {
        private readonly Player _player1;
        private readonly Player _player2;

        public Game(Player player1, Player player2)
        {
            _player1 = player1;
            _player2 = player2;
        }

        public enum Winner
        {
            Player,
            Map,
            None,
        };

        // Check if there is a winner, or not
        public Enum CheckWinner(Character[] playerUnits, Character[] mapUnits)
        {
            throw new NotImplementedException();
        }

        // Should run through a whole battle, return the logs of the battle and return the winning player!
        public (string[], Player) SimulateMatch()
        {
            throw new NotImplementedException();
        }

        // Based on the number of steps ran, it should decide to cast skill or do normal attack, and return the logs
        // for number of steps taken
        private string[] Step(int step, Character[] playerUnits, Character[] mapUnits)
        {
            throw new NotImplementedException();
        }
    }
}
namespace HumbleProgrammer.Characters
{
    public interface Character
    {
        string Name { get; }
        int Cooldown { get; }
        int Damage { get; }
        int Hp { get; set; }
        int Speed { get; }
        string[] SpecialAction(Character[] allies, Character[] enemies);
    }
}